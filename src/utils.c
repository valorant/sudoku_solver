#include <stdlib.h>
#include <stdio.h>

#include <math.h>

#include "sudoku.h"

__attribute__((noreturn)) void error(char *output)
{
    fprintf(stderr, "%s\n", output);
    exit(EXIT_FAILURE);
}

static void print_line(const sudoku_t sudoku, int nb_number);

void print_sudoku(sudoku_t sudoku)
{
    int count_x = 0;
    int count_y = 0;
    int number = 0;
    int nb_number = (int)log10(sudoku.number_of_number) + 1;
    print_line(sudoku, nb_number);
    for (size_t y = 0; y < sudoku.number_of_number; y++)
    {
        printf("\n| ");
        for (size_t x = 0; x < sudoku.number_of_number; x++)
        {
            number = get_number(x, y, sudoku);
            if (number != 0)
            {
                printf("%*d ", nb_number, get_number(x, y, sudoku));
            }
            else
            {
                for (size_t i = 0; i <= nb_number; i++)
                {
                    putchar(' ');
                }
            }
            count_x++;
            if (count_x >= sudoku.case_col && x != sudoku.number_of_number - 1)
            {
                printf("| ");
                count_x = 0;
            }
        }
        printf("|");
        count_x = 0;

        count_y++;
        if (count_y >= sudoku.case_line && y != sudoku.number_of_number - 1)
        {
            print_line(sudoku, nb_number);
            count_y = 0;
        }
    }
    print_line(sudoku, nb_number);
    putchar('\n');
}

static void print_line(const sudoku_t sudoku, int nb_number)
{
    printf("\n ");
    for (size_t j = 0; j < sudoku.case_line; j++)
    {
        for (size_t i = 0; i < sudoku.case_col; i++)
        {
            for (size_t j = 0; j <= nb_number; j++)
            {
                putchar('-');
            }
        }
        printf("- ");
    }
}