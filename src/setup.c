#include <stdlib.h>
#include <stdio.h>

#include "sudoku.h"
#include "utils.h"

#define BUFFER_SIZE 300
#define VALIDE_CHAR (c)(c <= '0' &&)

static FILE *sudoku_file;
static char err[100];

static void read_first_line(sudoku_t *sudoku);
static void create_table(sudoku_t *sudoku);
static int first_number_grid();

void parse_file(const char *filename, sudoku_t *sudoku)
{
    sudoku_file = fopen(filename, "r+");
    if (sudoku_file == NULL)
    {
        sprintf(err, "error: fopen(%s)", filename);
        error(err);
    }
    read_first_line(sudoku);
    create_table(sudoku);

    fclose(sudoku_file);
}

static void read_first_line(sudoku_t *sudoku)
{
    int my_char;
    int *number_change = &(sudoku->case_col);
    while ((my_char = fgetc(sudoku_file)) != '\n')
    {
        if (my_char == ' ')
        {
            number_change = &(sudoku->case_line);
        }
        else if (my_char >= '0' && my_char <= '9')
        {
            *number_change = (*number_change) * 10 + my_char - '0';
        }
        else
        {
            sprintf(err, "error: invalid character(%c)", my_char);
            error(err);
        }
    }
}

static void create_table(sudoku_t *sudoku)
{
    int number;
    sudoku->number_of_number = sudoku->case_col * sudoku->case_line;
    sudoku->grid = malloc(sizeof(int) * sudoku->number_of_number * sudoku->number_of_number);
    for (int j = 0; j < sudoku->number_of_number; j++)
    {
        for (int i = 0; i < sudoku->number_of_number; i++)
        {
            number = first_number_grid();
            if (number >= 0 && number <= sudoku->number_of_number)
            {
                sudoku->grid[i + j * sudoku->number_of_number] = number;
            }
            else
            {
                sprintf(err, "error: invalid number(%d)", number);
                error(err);
            }
        }
    }
}

static int first_number_grid()
{
    int my_char, number = 0;
    while ((my_char = fgetc(sudoku_file)) != ' ' && my_char != '\n' && my_char != -1)
    {
        if (my_char >= '0' && my_char <= '9')
        {
            number = number * 10 + my_char - '0';
        }
        else if (my_char == 'X')
        {
            number = 0;
        }
        else
        {
            sprintf(err, "error: invalid character(%c => %d)", my_char, my_char);
            error(err);
        }
    }
    return number;
}
