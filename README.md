# SUDOKU_SOLVER

![sudoku](screen/sudoku.png)

# USE

## Launch

```sh
cmake .
make
./sudoku_solver <filename>
```

## file structure

```
l c                         <- case dimention (line colunm)
X X X X ... X X
X X X X ... X X
...
X X X X ... X X             <- sudodoku data (X: unknown)
```
### Exemple :

![exemple](screen/file.png)

### results :

![results](screen/results.png)